# Plan 2022-02

## Now
- [x] https://gitlab.com/gitlab-org/gitlab/-/issues/352427+
- [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/348744+
- [ ] Personal development plan
   - [x] Apply to be a mentee
   - [x] LinkedIn Learning course
   - [ ] IGP
- [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/347510+
- [x] https://gitlab.com/gitlab-org/gitlab/-/issues/341739+
- [x] Coffee Chat: Reach out to your mentor and schedule a coffee chat, or chat async in Slack to connect, before 2022-03-11.
- [x] Program Kickoff: Attend a program kickoff session on 2022-03-10 or plan to contribute async in the [event issue](https://gitlab.com/gitlab-com/people-group/learning-development/mentorship/-/issues/10). (calendar invites to follow)
- [ ] Mentorship Set-up: Set your meeting cadence or async structure with your mentor: ~30 minutes, every other week, from March to July
- [x] Train: Complete the [How to be a good Mentor and Mentee training](https://about.gitlab.com/handbook/people-group/learning-and-development/mentor/#mentor-and-mentee-training) (LinkedIn and Text-Based training included in the handbook)
- [ ] Goal Setting: [Set a goal and purpose for your mentorship](https://about.gitlab.com/handbook/people-group/learning-and-development/mentor/#set-your-goal-and-purpose)
- [ ] Slack Communications: Star this #mentoring channel and follow posted updates throughout the program


## Next

- [ ] Recreate Import experience in Figma
   - [ ] Break it down first
- [ ] Recreate Optimize experience in Figma
   - [ ] Break it down first
- [ ] Set up reflective time
- [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/16975 potential for 14.10
- [ ] Import scorecard
- [ ] Add milestone table like Sunjung!
- [ ] https://www.linkedin.com/learning/making-user-experience-happen-as-a-team/welcome?u=2255073


## Later
- [ ] Document work done already
- [ ] Git 101
- [ ] Git 102
- [ ] Read Accelerate
- [ ] Fix Figma badge component in PJs
- [ ] Develop todos
- [ ] Create issue copy and paste template based on https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/#ux-definition-of-done-ux-dod 


## Done
- [x] Create skeleton Import figma file
   - https://www.figma.com/file/f1zOmjqyn4byN5RZumZWLK/Importers
- [x] Create skeleton Optimize figma file (show in sync)
   - https://www.figma.com/file/f4ax05m9prTMy9k7lkPFvy/Value-Stream-Analytics
   - https://www.figma.com/file/Q2NgMUH1OZH1198B8GrmTn/DevOps-Adoption
- [x] Figma Toggle fix
- [x] Re-read Values
